json.array!(@centers) do |center|
  json.extract! center, :id, :name, :status, :address, :country, :gmaps, :latitude, :longitude, :email, :phone_number, :description
  json.url center_url(center, format: :json)
end
