class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.string :name
      t.integer :user_id
      t.integer :game_id
      t.string :status
      t.string :added_by
      t.string :team

      t.timestamps null: false
    end

    add_index :reservations, :user_id
    add_index :reservations, :game_id
    add_index :reservations, [:user_id, :game_id], unique: true

  end
end
