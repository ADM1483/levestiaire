json.array!(@fields) do |field|
  json.extract! field, :id, :name, :description, :status
  json.url field_url(field, format: :json)
end
