class Terrain < ActiveRecord::Base

  resourcify

  has_many :matches
  has_one :category 
  belongs_to :user

  #Needed for image with paperclip
  has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  do_not_validate_attachment_file_type :image
  #validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

end
