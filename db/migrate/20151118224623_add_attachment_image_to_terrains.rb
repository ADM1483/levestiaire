class AddAttachmentImageToTerrains < ActiveRecord::Migration
  def self.up
    change_table :terrains do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :terrains, :image
  end
end
