class CentersController < ApplicationController
  before_action :set_center, only: [:show, :edit, :update, :destroy]

  # GET /centers
  # GET /centers.json
  def index
    @centers = Center.all
  end

  # GET /centers/1
  # GET /centers/1.json
  def show
  end

  # GET /centers/new
  def new
    @center = Center.new
  end

  # GET /centers/1/edit
  def edit
  end

  # POST /centers
  # POST /centers.json
  def create
    @center = Center.new(center_params)


    respond_to do |format|
      if @center.save

        if params[:images]
          params[:images].each do |image|
            @center.pictures.create(image: image)
          end
        end


        format.html { redirect_to @center, notice: 'Center was successfully created.' }
        format.json { render :show, status: :created, location: @center }
      else
        format.html { render :new }
        format.json { render json: @center.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /centers/1
  # PATCH/PUT /centers/1.json
  def update
    respond_to do |format|
      if @center.update(center_params)

        if params[:images]
          params[:images].each do |image|
            @center.pictures.create(image: image)
          end
        end

        format.html { redirect_to @center, notice: 'Center was successfully updated.' }
        format.json { render :show, status: :ok, location: @center }
      else
        format.html { render :edit }
        format.json { render json: @center.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /centers/1
  # DELETE /centers/1.json
  def destroy
    @center.destroy
    respond_to do |format|
      format.html { redirect_to centers_url, notice: 'Center was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_center
      @center = Center.find(params[:id])

      ### Pictures ###
      default_pic = Picture.find_by_description('default-pic')
      @logo = @center.pictures.find_by_description('logo')
      @cover = @center.pictures.find_by_description('cover')
      @pic1 = @center.pictures.find_by_description('pic1')
      @pic2 = @center.pictures.find_by_description('pic2')
      @pic3 = @center.pictures.find_by_description('pic3')

      # addign default_pic if @var don't have a picture
      # still need to test if pic.nil ? in case default_pic doesnt exist
      @logo ||= default_pic
      @cover ||= default_pic
      @pic1 ||= default_pic
      @pic2 ||= default_pic
      @pic3 ||= default_pic

      ### Fields ###
      @fields = @center.fields.all

      ### Categories ###
      @categories = @center.categories.all
      @categories.each do |cat|
        cat.picture ||= default_pic
      end

      ### Game ###
      @game = Game.new

      ### Chain ###
      # @chain = @center.chain

      ### Owner ###
      # @owner = @center.user
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def center_params
      params.require(:center).permit(:pictures, :name, :status, :address, :country, :gmaps, :latitude, :longitude, :email, :phone_number, :description)
    end
end
