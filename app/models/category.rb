class Category < ActiveRecord::Base
  has_many :terrains
  has_many :fields
  has_one :picture, as: :imageable, :dependent => :destroy
  has_many :rates, :dependent => :destroy
  belongs_to :center
end
