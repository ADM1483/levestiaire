/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*         Menu Burger Responsive              */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

$(document).ready(function(){
	$('#header-icon').click(function(e){
        e.preventDefault();
        $('body').toggleClass('with-sidebar');
    });
    $('#site-hidden').click(function(e){
        $('body').removeClass('with-sidebar');
    });
});


/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*           Search bar responsive             */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

$(document).ready(function(){
	$('#search-bar-responsive').click(function(e){
		e.preventDefault();
        $('body').toggleClass('with-search-modal');
	});
	$('#close-search-modal').click(function(e){
        $('body').removeClass('with-search-modal');
    });
});


/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*          Booking bar responsive             */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

$(document).ready(function(){
    $('#booking-bar-responsive').click(function(e){
        e.preventDefault();
        $('body').toggleClass('with-booking-modal');
    });
    $('#close-booking-modal').click(function(e){
        $('body').removeClass('with-booking-modal');
    });
});


/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*              Date-Time picker               */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

$(document).ready(function(){
    $('#datepicker').datetimepicker({
        format: 'L',
        minDate: 'now'
    });
    $('#timepicker').timepicker({
        'timeFormat': 'H:i',
        'scrollDefault': 'now',
        'minTime': '09:00',
        'maxTime': '23:30',
        'disableTextInput': 'true'
    });
});

$(document).ready(function(){
    $('#datepicker-responsive').datetimepicker({
        format: 'L',
        minDate: 'now'
    });
    $('#timepicker-responsive').timepicker({
        'timeFormat': 'H:i',
        'scrollDefault': 'now',
        'minTime': '09:00',
        'maxTime': '23:30',
        'disableTextInput': 'true'
    });
});


/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*               Scroll Book Box               */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
$(document).ready(function(){
    var FooterPosition = $('footer').offset().top;

    $(window).scroll(function() {
        if ($(window).scrollTop() > 100 && $(window).scrollTop() < FooterPosition) {
            $('#book-box').addClass("fixed");
        } else{
            $('#book-box').removeClass("fixed");
        }

    });
});
