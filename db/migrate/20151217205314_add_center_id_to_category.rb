class AddCenterIdToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :center_id, :integer
    add_index :categories, :center_id
  end
end
