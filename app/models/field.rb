class Field < ActiveRecord::Base
  belongs_to :center
  belongs_to :category
  has_many :games
  has_many :business_hours, as: :bh, :dependent => :destroy


end
