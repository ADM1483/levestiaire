class Center < ActiveRecord::Base
  has_many :pictures, as: :imageable, :dependent => :destroy
  belongs_to :user
  belongs_to :chain
  has_many :fields, :dependent => :destroy
  has_many :categories, :dependent => :destroy
  has_many :business_hours, as: :bh, :dependent => :destroy

end
