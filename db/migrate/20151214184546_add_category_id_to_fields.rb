class AddCategoryIdToFields < ActiveRecord::Migration
  def change
    add_column :fields, :category_id, :integer
    add_index :fields, :category_id
  end
end
