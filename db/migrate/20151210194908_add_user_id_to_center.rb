class AddUserIdToCenter < ActiveRecord::Migration
  def change
    add_column :centers, :user_id, :integer
    add_index :centers, :user_id
  end
end
