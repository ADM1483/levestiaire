# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151217205314) do

  create_table "business_hours", force: :cascade do |t|
    t.integer  "bh_id"
    t.string   "bh_type"
    t.string   "day"
    t.time     "start"
    t.time     "end"
    t.boolean  "open"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "business_hours", ["bh_id", "day"], name: "index_business_hours_on_bh_id_and_day", unique: true
  add_index "business_hours", ["bh_type", "bh_id"], name: "index_business_hours_on_bh_type_and_bh_id"
  add_index "business_hours", ["day"], name: "index_business_hours_on_day"

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "center_id"
  end

  add_index "categories", ["center_id"], name: "index_categories_on_center_id"

  create_table "centers", force: :cascade do |t|
    t.string   "name"
    t.string   "status"
    t.text     "address"
    t.string   "country"
    t.boolean  "gmaps"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "email"
    t.string   "phone_number"
    t.text     "description"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "user_id"
  end

  add_index "centers", ["user_id"], name: "index_centers_on_user_id"

  create_table "chains", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "fields", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "center_id"
    t.integer  "category_id"
  end

  add_index "fields", ["category_id"], name: "index_fields_on_category_id"
  add_index "fields", ["center_id"], name: "index_fields_on_center_id"

  create_table "games", force: :cascade do |t|
    t.string   "name"
    t.date     "day"
    t.time     "start"
    t.time     "end"
    t.float    "price"
    t.integer  "max_player"
    t.integer  "max_substitute"
    t.string   "payment"
    t.string   "status"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "field_id"
  end

  add_index "games", ["field_id"], name: "index_games_on_field_id"

  create_table "identities", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "accesstoken"
    t.string   "refreshtoken"
    t.string   "uid"
    t.string   "name"
    t.string   "email"
    t.string   "nickname"
    t.string   "image"
    t.string   "phone"
    t.string   "urls"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "identities", ["user_id"], name: "index_identities_on_user_id"

  create_table "matches", force: :cascade do |t|
    t.string   "description"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "user_id"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "terrain_id"
  end

  add_index "matches", ["terrain_id"], name: "index_matches_on_terrain_id"
  add_index "matches", ["user_id"], name: "index_matches_on_user_id"

  create_table "pictures", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  add_index "pictures", ["imageable_type", "imageable_id"], name: "index_pictures_on_imageable_type_and_imageable_id"

  create_table "rates", force: :cascade do |t|
    t.integer  "category_id"
    t.string   "day"
    t.time     "start"
    t.time     "end"
    t.float    "hourly_rate"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "rates", ["category_id"], name: "index_rates_on_category_id"

  create_table "relationships", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "relative_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "relationships", ["user_id", "relative_id"], name: "index_relationships_on_user_id_and_relative_id", unique: true
  add_index "relationships", ["user_id"], name: "index_relationships_on_user_id"

  create_table "reservations", force: :cascade do |t|
    t.string   "name"
    t.integer  "user_id"
    t.integer  "game_id"
    t.string   "status"
    t.string   "added_by"
    t.string   "team"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "reservations", ["game_id"], name: "index_reservations_on_game_id"
  add_index "reservations", ["user_id", "game_id"], name: "index_reservations_on_user_id_and_game_id", unique: true
  add_index "reservations", ["user_id"], name: "index_reservations_on_user_id"

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], name: "index_roles_on_name"

  create_table "terrains", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "user_id"
    t.integer  "center_id"
  end

  add_index "terrains", ["center_id"], name: "index_terrains_on_center_id"
  add_index "terrains", ["user_id"], name: "index_terrains_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "gender"
    t.text     "address"
    t.string   "phone_number"
    t.date     "birthdate"
    t.string   "level"
    t.string   "position"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"

end
