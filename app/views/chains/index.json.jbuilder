json.array!(@chains) do |chain|
  json.extract! chain, :id, :name, :description
  json.url chain_url(chain, format: :json)
end
