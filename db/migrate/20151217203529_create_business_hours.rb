class CreateBusinessHours < ActiveRecord::Migration
  def change
    create_table :business_hours do |t|
      t.references :bh, polymorphic: true, index: true
      t.string :day
      t.time :start
      t.time :end
      t.boolean :open

      t.timestamps null: false
    end
    add_index :business_hours, :day
    add_index :business_hours, [:bh_id, :day], unique: true
  end
end
