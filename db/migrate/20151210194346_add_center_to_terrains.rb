class AddCenterToTerrains < ActiveRecord::Migration
  def change
    add_column :terrains, :center_id, :integer
    add_index :terrains, :center_id
  end
end
