json.array!(@rates) do |rate|
  json.extract! rate, :id, :category_id, :day, :start, :end, :hourly_rate
  json.url rate_url(rate, format: :json)
end
