class Game < ActiveRecord::Base
  belongs_to :field
  has_many :reservations
  has_many :users, through: :reservations
end
