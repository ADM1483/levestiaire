class AddUsersParams < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :gender
      t.text :address
      t.string :phone_number
      t.date :birthdate
      t.string :level
      t.string :position
    end
  end
end
