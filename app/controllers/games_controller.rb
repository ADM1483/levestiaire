class GamesController < ApplicationController
  before_action :set_game, only: [:show, :edit, :update, :destroy, :join, :quit]

  # GET /games
  # GET /games.json
  def index
    @games = Game.all
  end

  # GET /games/1
  # GET /games/1.json
  def show
  end

  # GET /games/new
  def new
    @game = Game.new
  end

  # GET /games/1/edit
  def edit
  end

  # POST /games
  # POST /games.json
  def create
    @game = Game.new(game_params)

    respond_to do |format|
      if @game.save
        create_reservation(current_user, 'captain')

        format.html { redirect_to @game, notice: 'Game was successfully created.' }
        format.json { render :show, status: :created, location: @game }
      else
        format.html { render :new }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /games/1
  # PATCH/PUT /games/1.json
  def update
    respond_to do |format|
      if @game.update(game_params)
        format.html { redirect_to @game, notice: 'Game was successfully updated.' }
        format.json { render :show, status: :ok, location: @game }
      else
        format.html { render :edit }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  def join
    if  create_reservation(current_user, 'player')
      redirect_to @game, notice: 'You join the game'
    else redirect_to @game, notice: 'cannot join the game'
    end
  end

  def quit
    delete_reservation(current_user)
    redirect_to @game, notice: 'You quit the game'
  end


  # DELETE /games/1
  # DELETE /games/1.json
  def destroy
    @game.destroy
    respond_to do |format|
      format.html { redirect_to games_url, notice: 'Game was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game
      @game = Game.find(params[:id])

      if @game.reservations.any?
        # Captain = the one who create the game
        @captain = @game.reservations.find_by_status('captain').user
        # reservations of users who had confirmed
        @res_players = @game.reservations.where(status: 'player')

        @user_belongs_to_game = false
        @game.users.each do |user|
           if user.id == current_user.id
            @user_belongs_to_game = true
          end
        end

      end
    end

    def create_reservation(user, status)
      reservation = Reservation.create(game: @game, user: user, status: status)
    end

    def delete_reservation(user)
      Reservation.where(game_id: @game.id, user_id: user).first.destroy
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def game_params
      params.require(:game).permit(:name, :day, :start, :end, :price, :max_player, :max_substitute, :payment, :status)
    end
end
