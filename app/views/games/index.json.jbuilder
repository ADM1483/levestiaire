json.array!(@games) do |game|
  json.extract! game, :id, :name, :day, :start, :end, :price, :max_player, :max_substitute, :payment, :status
  json.url game_url(game, format: :json)
end
