class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :name
      t.date :day
      t.time :start
      t.time :end
      t.float :price
      t.integer :max_player
      t.integer :max_substitute
      t.string :payment
      t.string :status

      t.timestamps null: false
    end
  end
end
