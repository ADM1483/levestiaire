class AddFieldIdToGames < ActiveRecord::Migration
  def change
    add_column :games, :field_id, :integer
    add_index :games, :field_id
  end
end
