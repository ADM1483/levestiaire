class CreateRates < ActiveRecord::Migration
  def change
    create_table :rates do |t|
      t.integer :category_id
      t.string :day
      t.time :start
      t.time :end
      t.float :hourly_rate

      t.timestamps null: false
    end
    add_index :rates, :category_id
  end
end
