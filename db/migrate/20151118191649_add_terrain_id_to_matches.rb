class AddTerrainIdToMatches < ActiveRecord::Migration
  def change
    add_column :matches, :terrain_id, :integer
    add_index :matches, :terrain_id
  end
end
