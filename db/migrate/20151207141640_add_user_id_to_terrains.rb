class AddUserIdToTerrains < ActiveRecord::Migration
  def change
    add_column :terrains, :user_id, :integer
    add_index :terrains, :user_id
  end
end
