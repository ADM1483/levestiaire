json.array!(@business_hours) do |business_hour|
  json.extract! business_hour, :id, :field_id, :day, :start, :end, :open
  json.url business_hour_url(business_hour, format: :json)
end
