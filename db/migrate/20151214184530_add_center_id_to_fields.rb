class AddCenterIdToFields < ActiveRecord::Migration
  def change
    add_column :fields, :center_id, :integer
    add_index :fields, :center_id
  end
end
