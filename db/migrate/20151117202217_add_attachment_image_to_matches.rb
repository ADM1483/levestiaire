class AddAttachmentImageToMatches < ActiveRecord::Migration
  def self.up
    change_table :matches do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :matches, :image
  end
end
