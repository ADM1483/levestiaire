json.array!(@matches) do |match|
  json.extract! match, :id, :description, :terrain_id
  json.title "titre"
  json.start "2015-12-10T16:04:00.000Z"
  json.end "2015-12-10T17:04:00.000Z"
  json.url match_url(match, format: :html)
end
