class Chain < ActiveRecord::Base
  has_many :centers
  has_one :picture, as: :imageable, dependent: :destroy
end
