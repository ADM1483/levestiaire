class CreateCenters < ActiveRecord::Migration
  def change
    create_table :centers do |t|
      t.string :name
      t.string :status
      t.text :address
      t.string :country
      t.boolean :gmaps
      t.float :latitude
      t.float :longitude
      t.string :email
      t.string :phone_number
      t.text :description

      t.timestamps null: false
    end
  end
end
